# iotd

![](https://img.shields.io/badge/written%20in-PHP-blue)

An image gallery software.

Designed for high-volume, semi-anonymous LAN usage, the default configuration is to allow only LAN users to add images but for all internet users to view. To alleviate local bandwidth demands, images are lazily uploaded in the background to a third-party web host (in this case imgur.com).

Supports images, youtube videos, and flash files. This script was in use from circa 2009-2012.


## Download

- [⬇️ iotd-final.zip](dist-archive/iotd-final.zip) *(24.37 KiB)*
